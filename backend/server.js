const express = require('express')
const cors = require('cors')
const utils = require('./utils')
const dbconnect = require('./db')
const { request, response } = require('express')

const app = express()

app.use(cors('*'))
app.use(express.json())

app.get('/getmovie/:name',(request,response)=>{
    const{name}=request.params
    const connection=dbconnect.openDatabaseConnection()
    const statement=`SELECT * FROM movies WHERE movie_title='${name}'`
    connection.query(statement, (error,records)=>{
        response.send(utils.createResult(error,records))
    })
})

app.post('/addmovie',(request,response)=>{
    const{movie_title, movie_release_date,movie_time,director_name}=request.body
    const connection=dbconnect.openDatabaseConnection()
    const statement=`INSERT INTO movies(movie_title, movie_release_date,movie_time,director_name) 
        VALUES ('${movie_title}','${movie_release_date}','${movie_time}','${director_name}')
    `
    connection.query(statement, (error,records)=>{
        response.send(utils.createResult(error,records))
    })
})

app.put('/updatemovie/:id',(request,response)=>{
    const{id}=request.params
    const{movie_title, movie_release_date,movie_time,director_name}=request.body
    const connection=dbconnect.openDatabaseConnection()
    const statement=`UPDATE movies SET
        movie_release_date='${movie_release_date}',
        movie_time='${movie_time}'
        WHERE movie_id='${id}'
    `
    connection.query(statement, (error,records)=>{
        response.send(utils.createResult(error,records))
    })
})

app.delete('/deletemovie/:id',(request,response)=>{
    const{id}=request.params
    const connection=dbconnect.openDatabaseConnection()
    const statement=`DELETE FROM movies
        WHERE movie_id='${id}'
    `
    connection.query(statement, (error,records)=>{
        response.send(utils.createResult(error,records))
    })
})


app.listen(4000, '0.0.0.0', ()=>{
    console.log('Server started on port 4000.')
})