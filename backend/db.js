const mysql = require('mysql2')

function openDatabaseConnection(){
    const connection = mysql.createConnection({
        uri: 'mysql://db:3306',
        user: 'root',
        password: 'root',
        database: 'movieDB'
    })

    connection.connect()

    return connection
}

module.exports={
    openDatabaseConnection
}