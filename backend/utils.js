function createResult(error,records){
    const result = {}
    if(error){
        result['status']='error'
        result['error']=error
    }
    else{
        result['status']='success'
        result['data']=records
    }

    return result
}

module.exports = {
    createResult
}