-- Movie(movie_id, movie_title, movie_release_date,movie_time,director_name)
CREATE TABLE movies(
    movie_id INTEGER PRIMARY KEY auto_increment, 
    movie_title VARCHAR(300), 
    movie_release_date INTEGER,
    movie_time INTEGER,
    director_name VARCHAR(300)
)
